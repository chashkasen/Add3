#include <iostream>
#include "functions.hpp"

namespace km
{
	void Read(int mas[N], int& n)
	{
		std::cin >> n;
		for (int i = 0; i < n; i++)
			std::cin >> mas[i];

	}

	int Max(int mas[N], int n, int& max)
	{
		max = 0;

		for (int i = 0; i < n; i++)
			if (mas[i] > max)
				max = mas[i];
		return max;
	}

	int Min(int mas[N], int n, int& min)
	{
		min = 10000;

		for (int i = 0; i < n; i++)
			if (mas[i] < min)
				min = mas[i];
		return min;
	}

	void swap(int& a, int& b)
	{
		int tmp = a;
		a = b;
		b = tmp;
	}

	void sort(int mas[N], int n)
	{
		for (int i = 0; i < n - 1; i++)
			for (int j = i + 1; j < n; j++)
				if (mas[i] > mas[j])
					swap(mas[i], mas[j]);
	}

	void Write(int mas[N], int n, int a, int b)
	{
		for (int i = 0; i < n; i++)
			std::cout << mas[i] << " " << std::endl;

		std::cout << a << " " << b;
	}
}