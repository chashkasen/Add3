cmake_minimum_required(VERSION 3.22.1) 

set(PROJECT_NAME Add3)				 	 
project("${PROJECT_NAME}")


set(CMAKE_CXX_STANDARD 17)			 
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_CXX_STANDARD_REQUIRED TRUE)

include_directories("inc/")

add_executable("${PROJECT_NAME}" Add3.cpp
					inc/functions.hpp
					
					src/functions.cpp
)