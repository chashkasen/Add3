#pragma once
#include <iostream>
#define N 1000

namespace km
{
	void Read(int mas[N], int& n);


	int Max(int mas[N], int n, int& max);


	int Min(int mas[N], int n, int& min);


	void swap(int& a, int& b);

	void sort(int mas[N], int n);


	void Write(int mas[N], int n, int a, int b);
}